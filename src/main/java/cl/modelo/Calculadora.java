package cl.modelo;



public class Calculadora {
    private int capital;
    private int tasa_interes;
    private int anos;
    private float interes_producido;

    
    public int getCapital() {
        return capital;
    }

    
    public void setCapital(int capital) {
        this.capital = capital;
    }

   
    public int getTasa_interes() {
        return tasa_interes;
    }

  
    public void setTasa_interes(int tasa_interes) {
        this.tasa_interes = tasa_interes;
    }

   
    public int getAnos() {
        return anos;
    }

  
    public void setAnos(int anos) {
        this.anos = anos;
    }

    
    public float getInteres_producido() {
        int porcentaje=this.tasa_interes/100;
        float I=porcentaje*this.capital*this.anos;
        return I;
    }

   
    public void setInteres_producido(float interes_producido) {
        this.interes_producido = interes_producido;
    }
}
